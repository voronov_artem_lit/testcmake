cmake_minimum_required(VERSION 3.1) 
project(dev_03_subproject)

set(SOURCE_EXE main.cpp)

include_directories(foo)			# Расположение заголовочных файлов

add_executable(main ${SOURCE_EXE})

add_subdirectory(foo)				# !!! Добавление подпроекта, указывается имя дирректории

target_link_libraries(main foo)